package net.joaoqalves.domain;

import net.joaoqalves.TestContext;
import net.joaoqalves.domain.catalogers.TitleQualifier;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TitleQualifierTest extends TestContext {

    @Test
    public void isMarfeelizable() {
        TitleQualifier t = new TitleQualifier();

        assertTrue(t.isMarfeelizable(Optional.ofNullable(mockDocument1)));
        assertTrue(t.isMarfeelizable(Optional.ofNullable(mockDocument2)));
        assertFalse(t.isMarfeelizable(Optional.ofNullable(mockDocument3)));
    }

}
