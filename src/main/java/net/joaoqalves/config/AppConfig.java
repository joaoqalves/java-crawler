package net.joaoqalves.config;

import net.joaoqalves.domain.catalogers.Qualifier;
import net.joaoqalves.domain.catalogers.TitleQualifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ComponentScan(basePackages = "net.joaoqalves")
@EnableAsync
public class AppConfig {

    @Autowired
    private TitleQualifier titleCataloger;

    @Bean
    List<Qualifier> catalogers() {
        List<Qualifier> marfeableQualifiers = new ArrayList<>();
        marfeableQualifiers.add(titleCataloger);
        return marfeableQualifiers;
    }

}
