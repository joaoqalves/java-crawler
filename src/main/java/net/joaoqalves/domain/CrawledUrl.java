package net.joaoqalves.domain;

import org.eclipse.jetty.http.HttpStatus;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Optional;

@Document(collection = "crawledurls")
public class CrawledUrl {

    @Id
    private String id;
    private String url;
    private Optional<HttpStatus.Code> statusCode;
    private Optional<String> error;
    private boolean marfeelizable;

    public CrawledUrl(String url, Optional<HttpStatus.Code> statusCode, Optional<String> error, boolean marfeelizable) {
        this.url = url;
        this.marfeelizable = marfeelizable;
        this.statusCode = statusCode;
        this.error = error;
    }

    public CrawledUrl() {
    }

    public String getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isMarfeelizable() {
        return marfeelizable;
    }

    public void setMarfeelizable(boolean marfeable) {
        this.marfeelizable = marfeable;
    }
}
