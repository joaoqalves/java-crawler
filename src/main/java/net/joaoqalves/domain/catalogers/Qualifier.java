package net.joaoqalves.domain.catalogers;

import org.jsoup.nodes.Document;

import java.util.Optional;

public interface Qualifier {

    boolean isMarfeelizable(final Optional<Document> document);

}
